#include "model/person_impl.h"
#include <stdlib.h>
#include <assert.h>
#include <string.h>

int _person_sprintf(char *output, Person *person) {
    const char *first_name = person_first_name(person);
    const char *last_name = person_last_name(person);
    const int personAge = get_age(person);

    return sprintf(output, "%s,%s,%d", first_name, last_name, personAge);
}

const char* _first_name(Person *person) {
    return person->first_name;
}

const char* _last_name(Person *person) {
    return person->last_name;
}

const int _age(Person *person) {
    return person->age;
}

void _delete_person(Person* person) {
    free(person);
}

Person * new_person(const char *restrict first_name, const char *restrict last_name, const int age) {
    Person *person = malloc(sizeof(Person));

    //attributes
    strcpy(person->first_name, first_name);
    strcpy(person->last_name, last_name);
    person->age = age;
    
    // methods
    person->get_first_name = _first_name;
    person->get_last_name = _last_name;
    person->get_age = _age;
    person->sprintf = _person_sprintf;
    person->delete = _delete_person;
    return person;
}

const char * person_first_name(Person *person) {
    return person->get_first_name(person);
}

const char * person_last_name(Person *person) {
    return person->get_last_name(person);
}

const int get_age(Person *person) {
    return person->get_age(person);
}

int person_sprintf(char *output, Person *person) {
    return person->sprintf(output, person);
}

void delete_person(Person *person) {
    person->delete(person);
}