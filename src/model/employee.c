#include "model/person_impl.h"
#include "model/employee.h"
#include <string.h>
#include <assert.h>

int _employee_sprintf(char *output, Person *person);

void _delete_employee(Person *employee);

typedef struct employee {
    // attributes
    Person *super;
    char department[64];

    // methods
    int (*super_person_sprintf)(char *, Person *);
    void (*super_person_delete)(Person *);
} Employee;

Person* new_employee(const char *restrict first_name, const char *restrict last_name, const int age, const char *restrict department) {
    Person *super = new_person(first_name, last_name, age);
    Employee *employee = malloc(sizeof(Employee));

    // Attributes
    strcpy(employee->department, department);
    super->this = employee;
    employee->super = super;

    // Method overriding
    employee->super_person_sprintf = super->sprintf;
    employee->super_person_delete = super->delete;
    super->sprintf = _employee_sprintf;
    super->delete = _delete_employee;

    return employee->super;
}

Employee * cast_to_employee(Person *person) {
    return (Employee *) person->this;
}

const char * employee_department(Employee *employee) {
    return employee->department;
}

int _employee_sprintf(char *output, Person *person) {
    Employee* employee = cast_to_employee(person);
    
    char person_result[256];

    int result = employee->super_person_sprintf(person_result, person);

    result += sprintf(output, "%s,%s", person_result, employee_department(employee));
    
    return result;
}

void _delete_employee(Person *employee) {
    Employee *_cast_employee = cast_to_employee(employee);
    _cast_employee->super_person_delete(_cast_employee->super);
    free(_cast_employee);
}