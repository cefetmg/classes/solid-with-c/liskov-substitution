#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "model/person.h"
#include "model/employee.h"
#include "service/person_compare.h"
#include "service/person_writer.h"

int _polymorphic_name_compare(void const*, void const*);

int main(void) {

    Person *persons[] = {
        new_person("Karen", "Goncalves", 25),
        new_employee("Rodrigo", "Novaes", 26, "Software Engineering"),
        new_employee("Felipe", "Reis", 27, "Masters Research"),
        new_person("Ryan", "Novaes", 18),
        new_person("Charlotte", "Novaes", 21),
        new_employee("Bruno", "Menezes", 28, "Maintenance"),
        new_person("Rodrigo", "Mascarenhas", 25),
    };
    const unsigned long amount = sizeof(persons)/sizeof(Person *);

    printf("first name,last name,age,department\n");
    persons_fprintf(amount, persons, stdout);
    
    qsort(persons, amount, sizeof(Person *), &_polymorphic_name_compare);
    printf("***\n");
    
    printf("first name,last name,age,department\n");
    persons_fprintf(amount, persons, stdout);

    for (int i = 0; i < amount; i++) {
        delete_person(persons[i]);
    }

    return 0;
}

int _polymorphic_name_compare(void const *s, void const *t) {
    Person *cast_s = *(Person **) s;
    Person *cast_t = *(Person **) t;
    return person_compare(cast_s, cast_t);
}