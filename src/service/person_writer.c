#include "service/person_writer.h"

void person_fprintf(Person * const person, FILE *const file) {
    char output[256];
    person_sprintf(output, person);
    fprintf(file, "%s", output);
}

void persons_fprintf(int const size, Person **persons, FILE * const file) {
    int i;
    for(i = 0; i < size; i++) {
        person_fprintf(persons[i], file);
        fprintf(file, "\n");
    }
}