#include "service/person_compare.h"
#include <string.h>
#include <assert.h>

int person_compare(Person * const s, Person * const t) {
    int result = strcmp(person_first_name(s), person_first_name(t));

    if (!result) {
        result = strcmp(person_last_name(s), person_last_name(t));
    }

    return result;
}