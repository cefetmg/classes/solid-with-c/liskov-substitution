#ifndef MODEL_PERSON_IMPL_H
#define MODEL_PERSON_IMPL_H

#include "model/person.h"

typedef struct person {
    // attributes
    void *this;
    char first_name[64];
    char last_name[64];
    int age;

    // methods
    int (*sprintf)(char *output, Person *);
    const char * (*get_first_name)(Person *);
    const char * (*get_last_name)(Person *);
    const int (*get_age)(Person *);
    void (*delete)(Person *);
} Person;

#endif