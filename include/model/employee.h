#ifndef MODEL_EMPLOYEE_H
#define MODEL_EMPLOYEE_H

#include <stdio.h>
#include <stdlib.h>
#include "model/person.h"

typedef struct employee Employee;

Person * new_employee(const char *restrict first_name, const char *restrict last_name, const int age, const char *restrict department);

Employee * cast_to_employee(Person *);

const char * employee_department(Employee *employee);

#endif