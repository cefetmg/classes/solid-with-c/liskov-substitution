#ifndef MODEL_PERSON_H
#define MODEL_PERSON_H

#include <stdio.h>
#include <stdlib.h>

typedef struct person Person;

Person * new_person(const char *restrict first_name, const char *restrict last_name, const int age);

const char * person_first_name(Person *);

const char * person_last_name(Person *);

const int get_age(Person *);

int person_sprintf(char *output, Person *);

void delete_person(Person *person);

#endif