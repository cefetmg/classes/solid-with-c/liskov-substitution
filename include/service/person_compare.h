
#ifndef SERVICE_PERSON_COMPARE_H
#define SERVICE_PERSON_COMPARE_H

#include "model/person.h"

int person_compare(Person * const, Person * const);

#endif