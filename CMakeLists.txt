cmake_minimum_required(VERSION 3.10)

project(
  "Single-Responsibility with C"
  VERSION 0.1.0
  DESCRIPTION "Getting to learn SOLID with C"
  LANGUAGES C)

add_library(
  person_model STATIC include/model/person.h 
                      include/model/person_impl.h 
                      src/model/person.c)
target_include_directories(person_model PUBLIC include)

add_library(
  employee_model STATIC include/model/person_impl.h
                        include/model/employee.h
                        src/model/employee.c)
target_include_directories(employee_model PUBLIC include)

add_library(person_compare STATIC src/service/person_compare.c
                                  include/service/person_compare.h)
target_include_directories(person_compare PUBLIC include)

add_library(person_writer STATIC src/service/person_writer.c
                                 include/service/person_writer.h)
target_include_directories(person_writer PUBLIC include)

add_executable(person_main src/main.c)

target_link_libraries(person_main PUBLIC person_model employee_model
                                         person_compare person_writer)

enable_testing()

include(CTest)

add_test(liskov_substitution_test person_main)